'use strict';
const expect = require('chai').expect;
const FizzBuzzService = require('../services/FizzBuzzService');
const fizzBuzzConstants = require('../constants/fizzBuzzConstants');

describe('FizzBuzzService', function () {
  it('should exist', function () {
    expect(FizzBuzzService).to.not.be.undefined;
  });

  it('should set start and end properties when instantiated', function () {
    let fizzBuzzService = new FizzBuzzService(1, 20);

    expect(fizzBuzzService.start).to.equal(1);
    expect(fizzBuzzService.end).to.equal(20);

    fizzBuzzService = new FizzBuzzService(5, 10);

    expect(fizzBuzzService.start).to.equal(5);
    expect(fizzBuzzService.end).to.equal(10);
  });
});

describe('getNumberText', function () {
  it('should return fizz for mulitples of 3', function () {
    let text = FizzBuzzService.getNumberText(6);

    expect(text).to.equal(fizzBuzzConstants.fizz);

    text = FizzBuzzService.getNumberText(9);

    expect(text).to.equal(fizzBuzzConstants.fizz);
  });

  it ('should return buzz for multiples of 5', function () {
    let text = FizzBuzzService.getNumberText(5);

    expect(text).to.equal(fizzBuzzConstants.buzz);

    text = FizzBuzzService.getNumberText(10);

    expect(text).to.equal(fizzBuzzConstants.buzz);
  });

  it('should return fizzbuzz for multiples of 15', function () {
    let text = FizzBuzzService.getNumberText(15);

    expect(text).to.equal(fizzBuzzConstants.fizzbuzz);

    text = FizzBuzzService.getNumberText(45);

    expect(text).to.equal(fizzBuzzConstants.fizzbuzz);
  });

  it('should return the number for numbers that are not multiples of 3, 5 or 15', function () {
    let text = FizzBuzzService.getNumberText(1);

    expect(text).to.equal(1);

    text = FizzBuzzService.getNumberText(2);

    expect(text).to.equal(2);
  });

  it('should return lucky if the number contains a 3', function () {
    let text = FizzBuzzService.getNumberText(3);

    expect(text).to.equal(fizzBuzzConstants.lucky)
  });
});

describe('getSequence', function () {
  it('should output a sequence in the fizzbuzz format', function () {
    let fizzBuzzService = new FizzBuzzService(1, 20);
    let sequence = fizzBuzzService.getSequence();

    expect(sequence).to.equal('1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz');

    fizzBuzzService = new FizzBuzzService(20, 40);
    sequence = fizzBuzzService.getSequence();

    expect(sequence).to.equal('buzz fizz 22 lucky fizz buzz 26 fizz 28 29 lucky lucky lucky lucky lucky lucky lucky lucky lucky lucky buzz');
  });
});

describe('containsNumber', function () {
  it('should return true if number parameter contains the searchNumber parameter', function () {
    let found = FizzBuzzService.containsNumber(1234, 3);

    expect(found).to.be.true;

    found = FizzBuzzService.containsNumber(35, 3);

    expect(found).to.be.true;
  });

  it('should return false if number parameter does not contain the searchNumber parameter', function () {
    let found = FizzBuzzService.containsNumber(1234, 5);

    expect(found).to.be.false;

    found = FizzBuzzService.containsNumber(957362, 1);

    expect(found).to.be.false;
  });
});

describe('getReport', function () {
  it('should output a report in the correct format', function () {
    let fizzBuzzService = new FizzBuzzService(1, 20);
    let sequence = fizzBuzzService.getSequence();
    let report = FizzBuzzService.getReport(sequence);

    expect(report).to.equal('fizz: 4 buzz: 3 fizzbuzz: 1 lucky: 2 integer: 10');

    fizzBuzzService = new FizzBuzzService(20, 40);
    sequence = fizzBuzzService.getSequence();
    report = FizzBuzzService.getReport(sequence);

    expect(report).to.equal('fizz: 3 buzz: 3 fizzbuzz: 0 lucky: 11 integer: 4')
  });
});

describe('getOutput', function () {
  it('should output sequence and report in the correct format', function () {
    let fizzBuzzService = new FizzBuzzService(1, 20);
    let output = fizzBuzzService.getOutput();

    expect(output).to.equal('1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz fizz: 4 buzz: 3 fizzbuzz: 1 lucky: 2 integer: 10');

    fizzBuzzService = new FizzBuzzService(20, 40);
    output = fizzBuzzService.getOutput();

    expect(output).to.equal('buzz fizz 22 lucky fizz buzz 26 fizz 28 29 lucky lucky lucky lucky lucky lucky lucky lucky lucky lucky buzz fizz: 3 buzz: 3 fizzbuzz: 0 lucky: 11 integer: 4');
  });
});