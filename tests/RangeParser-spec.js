const expect = require('chai').expect;
const RangeParserService = require('../services/RangeParserService');

describe('RangeParserService', function () {
  it('should exist', function () {
    expect(RangeParserService).to.not.be.undefined;
  })
});

describe('getRange', function () {
  it('should return an array of integers', function () {
    let range = RangeParserService.getRange('1-20');

    expect(range).to.eql([1, 20]);

    range = RangeParserService.getRange('5-15');

    expect(range).to.eql([5, 15]);
  })

  it('should return an error if range is not in required format', function () {
    expect(() => RangeParserService.getRange('1-20-30')).to.throw();
  });

  it('should return an error if range contains a value that is not an integer', function () {
    expect(() => RangeParserService.getRange('1-test')).to.throw();
  });

  it('should return an error if second value in range is less than the first value', function () {
    expect(() => RangeParserService.getRange('20-1')).to.throw();
  });
});