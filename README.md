# Real FizzBuzz

A program which, given a contiguous range of integers, prints an output in the fizzbuzz format.

## Requirements

To run this program version 6.4.0 or later of [Node.js](https://nodejs.org/) must be installed.

## Setup

Run `npm install` to install the package dependencies for the project.

## Running the program

Run `node index.js [range]` to print the output for the provided range argument. The range argument must be in the format `[integer]-[integer]` e.g. `node index.js 1-20`.

## Running the tests

Run `npm test` to run the tests for the project.

## Version

This program was developed based upon version `925cf7f189d1e404cfd55df6e8398d831df0f45a` of the instructions from https://equalexperts.github.io/ee-fizzbuzz-marking/candidate-instructions.