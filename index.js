'use strict';
const FizzBuzzService = require('./services/FizzBuzzService');
const RangeParserService = require('./services/RangeParserService');

const rangeArg = process.argv[2];

if (!rangeArg) {
  throw new Error('No range argument provided. E.g. 1-20');
}

// Parse the range integers from the provided argument.
const range = RangeParserService.getRange(rangeArg);

// Instantiate a FizzBuzz object with the range of numbers.
const fizzBuzzService = new FizzBuzzService(range[0], range[1]);

// Output the text for the specified range of numbers.
console.log(fizzBuzzService.getOutput());