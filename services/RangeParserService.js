'use strict';

module.exports = class {
  static getRange(rangeStr) {
    // Get values from the argument.
    let values = rangeStr.split('-');

    // Throw error if range is not provided in the required format.
    if (!values || values.length != 2) {
      throw new Error('Provided range not in required format. E.g. 1-20.');
    }

    // Loop through the values and convert to integers.
    for (let i = 0; i < values.length; i++) {
      const value = parseInt(values[i]);

      // Throw error is value from range cannot be parsed into an integer.
      if (isNaN(value)) {
        throw new Error('Provided range contains a value that is not an integer.');
      }

      values[i] = value;
    }

    // THrow error if the second value in the range is less than the first value.
    if (values[1] < values[0]) {
      throw new Error('Second value in provided range must be greater than the first.')
    }

    return values;
  }
};