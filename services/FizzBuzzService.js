'use strict';
const fizzBuzzConstants = require('../constants/fizzBuzzConstants');

module.exports = class FizzBuzzService {
  constructor(start, end) {
    this.start = start;
    this.end = end;
  }

  static getNumberText(number) {
    if (FizzBuzzService.containsNumber(number, 3)) {
      return fizzBuzzConstants.lucky;
    } else if (number % 15 === 0) {
      // Return 'fizzbuzz' if number is multiple of 15.
      return fizzBuzzConstants.fizzbuzz
    } else if (number % 5 === 0) {
      // Return 'buzz' if number is multiple of 5.
      return fizzBuzzConstants.buzz;
    } else if (number % 3 === 0) {
      // Return 'fizz' if number is multiple of 3.
      return fizzBuzzConstants.fizz;
    }

    // Return the number itself is not one of the multiples above.
    return number;
  }

  static containsNumber(number, searchNumber) {
    // Convert the numbers to strings so they can be used to search.
    const numberStr = number.toString();
    const searchNumberStr = searchNumber.toString();

    // Check if the number contains the search number.
    if (numberStr.indexOf(searchNumberStr) > -1) {
      return true;
    }

    return false;
  }

  static getReport(sequence) {
    let output = '';

    // Initialize the report with the relevant categories.
    let report = {
      fizz: 0,
      buzz: 0,
      fizzbuzz: 0,
      lucky: 0,
      integer: 0
    };

    // Split the sequence into the individual parts.
    const parts = sequence.split(' ');

    // Loop through the parts and increment the relevant report categories.
    for (let i = 0; i < parts.length; i++) {
      if (isNaN(parseInt(parts[i]))) {
        // Part is not a number so increment the relevant report category.
        report[parts[i]] += 1;
      } else {
        // Part is a number so increment the 'integer' report category.
        report['integer'] += 1;
      }
    }

    // Loop through the report categories and append them to the output.
    const reportCategories = Object.keys(report);

    for (let i = 0; i < reportCategories.length; i++) {
      output += `${reportCategories[i]}: ${report[reportCategories[i]]}`;

      // Append a space if not the last category.
      if (i < reportCategories.length - 1) {
        output += ' ';
      }
    }

    return output;
  }

  getSequence() {
    let sequence = '';

    // Loop through the numbers in the range, get the appropriate text and append to the output.
    for (let i = this.start; i <= this.end; i++) {
      // Append text to output.
      sequence += FizzBuzzService.getNumberText(i);

      // Append a space if not the last number.
      if (i < this.end) {
        sequence += ' ';
      }
    }

    return sequence;
  }

  getOutput() {
    const sequence = this.getSequence();
    const report = FizzBuzzService.getReport(sequence);

    return `${sequence} ${report}`;
  }
}