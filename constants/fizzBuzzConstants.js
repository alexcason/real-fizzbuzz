module.exports = {
  fizz: 'fizz',
  buzz: 'buzz',
  fizzbuzz: 'fizzbuzz',
  lucky: 'lucky'
};